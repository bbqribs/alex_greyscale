
/* global THREE, posx */
$(window).resize(function(){
    onWindowResize();
}); //handles resizing window when browser is resized in client

/*Handles window resizes. */
function onWindowResize(){
    
//    console.log(camera);
    
    var w = $(window).innerWidth();
    var h = $(window).innerHeight();
    /*For the ortho camera*/
    if (camera.hasOwnProperty("left")){
        camera.left = w / - camerascalar;
        camera.right = w  / camerascalar;
        camera.top = h / camerascalar;
        camera.bottom = h / - camerascalar;
    }
    /*For the perspective camera*/
    else if (camera.hasOwnProperty("apsect")){        
        camera.aspect = w/h;
    }
    
    camera.updateProjectionMatrix();
    
    renderer.setSize( window.innerWidth, window.innerHeight );
}

/***DETECTOR****/
if (!Detector.webgl) {
    
    var body = $('body');
    body.text("Sorry, your browser doesn't support WebGL!");
    body.append("<br><a href=\"http://get.webgl.org\"> Click here to get webGL poppin'</a>");
    body.css("text-align", "center");
    body.css("margin-top", "10%");
    body.css("font-size", "20px");
    
    $("#three-canvas").remove();
    $("#controls").remove();
}

var w = $(window).innerWidth();
var h = $(window).innerHeight();


/**********************************Necessary THREE.JS setup things*************************************/
/*Create the renderer, set its size and add it to the page*/
var renderer = new THREE.WebGLRenderer();
//var renderer2D = new THREE.CanvasRenderer();
renderer.setSize(w, h);

var canvas = $('#three-canvas');
//var canvas2D = $('2dcanvas');
canvas.append(renderer.domElement);
//canvas2D.append(renderer2D.domElement);

/*Create camera (orthographic means you DONT have FORESHORTENING)*/
var camerascalar = 175;
var camera = new THREE.OrthographicCamera( w / - camerascalar, w / camerascalar, h / camerascalar, h / - camerascalar, 1, 1000); //Frustrum lol

var fov = 100;

//var camera = new THREE.PerspectiveCamera(fov, w/h, 0.1, 1000);
camera.position.z = 7;
//camera.position.x = -5;

/*Create scene/stage*/
var scene = new THREE.Scene();

/*Create a light source*/
var light = new THREE.AmbientLight( new THREE.Color(1, 1, 1) );
scene.add(light);




/**********************WORKSPACE**************************/
function setStyle()
{
    //sets display for divs that can be toggled.
    var a = document.getElementById('gobar');
    var b = document.getElementById('instruct1');
    var c = document.getElementById('instruct2');
    var d = document.getElementById('splashscreen');
    var elementlist = [a,b,c];
    for(var e of elementlist)
    {
        e.style.display = "none";
    }
    d.style.display = "block";
}
init();
function init()
{
    globalproperties = {}; //assign globalproperties variable as empty object
    //initialising parameters for modules
    maininit();
    resultsinit();
    testarrayinit();
    setStyle();
    // ask for participant initials
     var subject = prompt('Please enter your initials');
    changeglobal('subject', subject);
    //creates array holding filenames for image loading
    var patharray = createPatharray();
    var patharray2 = createPatharray2();
    //load textures
    loadTextures("img/greyscale", patharray).then(function(texturearray){
        //then store textures
        var materialarray = [];
        for(let val of texturearray)
        {
            var imageMaterial = new THREE.MeshBasicMaterial({
                map: val,
                color: new THREE.Color(1,1,1)
            });
            imageMaterial.name = val.name;
            materialarray.push(imageMaterial);
        }
        textures = materialarray;
        //then load second set of textures
        }).then(loadTextures.bind( null, "img/greyscale", patharray2)).then(function(texturearray){
        for(let val of texturearray)
        {
            //then store them under textures as well
            var imageMaterial = new THREE.MeshBasicMaterial({
                map: val,
                color: new THREE.Color(1,1,1)
            });
            imageMaterial.name = val.name;
            textures.push(imageMaterial);
        }    
        //then load fonts
        }).then(loadFont.bind( null ,"font/Arialic_Hollow_Regular.json")).then(function(font){
            //then save under loadedFont
            console.log('2',font);
            loadedFont = font;
            //then show gobar, allow experiment to start
            toggleVisibility('gobar');
            changeglobal('splash',false);
    });
}
//run initialisation function

function maininit()
{
    globalproperties['main'] = {
        num: 0,     //track trial number
        repeat: 0,  //track number of repeated runs
        numRepeat: 1,   //number of desired repeats
        baseLength: 4,  //default length of bar
        barHeight: 1,   //default height of bars
        alphaString: 'abcdefghijklmnopqrstuvwxyz',
        splash:true,    //displays splash screen on load
        run:false,      //stops running of exp until all assets are loaded
        task: 0,    //tracks which task is to be displayed. 0==letter task, 1==actual task
    };
    globalproperties['positions'] = {
        //positions of greyscale bars
        top: {x:0, y:2},
        bot: {x:0, y:-2}
    };
    globalproperties['trialdata'] = {
        //tracks data of current trial
        luminance: .5,
        condition: null,
        whereleft: null,
        whereluminance: null,
    };
    globalproperties['trialtimer']={
        trialStartTime: 0,  //tracks time of trial start, prevents spamming of responses
        minTime: 3000,  //minimum display time of greyscale bars
        maxTime: 5000,  //maximum display time of bars
    };
}
/* creates patharrays for loading textures*/
function createPatharray()
{
    var alphaString = 'abcdefghijklmnopqrstuvwxyz';
    var numVariations = 10;
    var filetype = '.bmp';
    var multiplier = ['8','9'];
    var patharray = [];
    for(let idx of multiplier)
    {
        for(let i=0; i<numVariations; i++)
        {
            let variation = alphaString[i];
            var tempstring = idx + variation + filetype;         
            patharray.push(tempstring);
        }
    }
    return patharray;
}

function createPatharray2()
{
    var alphaString = 'abcdefghijklmnopqrstuvwxyz';
    var numVariations = 5;
    var filetype = '.bmp';
    var multiplier = ['8','9'];
    var luminance = ['dark', 'light'];
    var patharray = [];
    for (let ligs of luminance)
    {
        for(let idx of multiplier)
        {
            for(let i=0; i<numVariations; i++)
            {
                let variation = alphaString[i];
                var tempstring = idx + variation + ligs + filetype;         
                patharray.push(tempstring);
            }
        }
    }
    return patharray;
}

/*create background for experiment*/
function createSuperbackground()
{
    //creates grey backdrop for experiment
    var geometry = new THREE.PlaneGeometry( w, h );
    var val = .5; //colour value
    var properties = {
        color: new THREE.Color(val, val, val) //can set any RGB colour value instead of val
    };
    var material = new THREE.MeshBasicMaterial( properties ); 
    var background = new THREE.Mesh( geometry, material ); 
    background.position.x = 0;
    background.position.y = 0;
    background.position.z = -6;
    background.name = 'superbackground';
    scene.add( background ); 
}
createSuperbackground();

/*starting up experiment*/
function startup()
{
    createTestArray();  //creates test data array    
//    midlineAxes(); //displays axes to help position things on screen
    createOutput(globallookup('data'),globallookup('headings'));
    createtrial();
}

function createtrial()
{
    //grabs trial data for current trial and passes them to functions that make stimulus
    var data = globallookup('data');
    var num = globallookup('num');
    var currdata = data[num];
    var positions = globallookup('positions');
    makeDisplay(currdata, positions);
    createOverlay(currdata, positions);
}

function makeDisplay(data, position)
{
    //takes trial data and bar position, creates and positions bar
    var texturearray = findtextures(data); //returns random variant of texture
    if (data.control !== 0)
    {
        var texture2 = findtextures(data, data.control);
//        console.log(texture2);
    }
//    console.log(texturearray)
    var baselength = globallookup('baseLength');
    var imagescalar = data.length;
    var length = imagescalar*baselength;
    //randomly allocates position of bars
    var allocatePositions = whereisleft();
    var allocateleftluminance = data.control !== 0?whereisleft():null;
    changeglobal('whereluminance', allocateleftluminance);
    changeglobal('whereleft', allocatePositions);
    if (allocatePositions === 0)
    {
        var leftdark = allocateleftluminance === 1?createTexturePatch(length, texture2, position.top):createTexturePatch(length, texturearray, position.top);
        var rightdark = allocateleftluminance === 0?createTexturePatch(length, texture2, position.bot):createTexturePatch(length, texturearray, position.bot);
    }
    else if (allocatePositions === 1)
    {
        var leftdark = allocateleftluminance === 1?createTexturePatch(length, texture2, position.bot):createTexturePatch(length, texturearray, position.bot);
        var rightdark =  allocateleftluminance === 0?createTexturePatch(length, texture2, position.top):createTexturePatch(length, texturearray, position.top);
    }
    //creating black outline for bars
    var outlineTop = outlineGreyscale(length, position.top, imagescalar);
    var outlineBot = outlineGreyscale(length, position.bot, imagescalar);
    rightdark.rotation.z = Math.PI; //invert one of the bars
    //adds bars and outlines to scene
    scene.add(leftdark,rightdark);
    scene.add(outlineTop, outlineBot);
    //sets trial start time to now.
    changeglobal('trialStartTime', Date.now());
}

function outlineGreyscale(length, posobj, imagescalar)
{
    //creates black outline
    var height = globallookup('barHeight');   
    var scale = .05;
    var geometry = new THREE.PlaneGeometry(length, height);
    var properties  = {
        color: new THREE.Color(0,0,0),
    };
    var material = new THREE.MeshBasicMaterial(properties);
    var mesh = new THREE.Mesh(geometry, material);
    mesh.position.x = posobj.x;
    mesh.position.y = posobj.y;
    mesh.position.z = -1;
    mesh.name = "wipe";
    mesh.scale.x += scale/(imagescalar*4);
    mesh.scale.y += scale;
    return mesh;
}

function whereisleft()
{
    var rand = Math.random();
    if(rand < .5)
    {
        return 0; //top
    }
    else 
    {
        return 1; //bottom
    }
    
}

function createOverlay(data, position)
{
    //creates text overlay
    var height = globallookup('barHeight');
    var baselength = globallookup('baseLength');
    //get number of required letters
    var length = baselength * data.length;
    var num = Math.floor(length);
    var textoverlay = createTextArray(data.letter, num, height);
    if(data.orientation === 1)
    {
        textoverlay.rotation.y += Math.PI;
    }
    textoverlay.position.y = position.top.y;
    textoverlay.position.x = position.top.x;
    textoverlay.position.z = 1;
    scene.add(textoverlay.clone());
    textoverlay.position.y = position.bot.y;
    textoverlay.position.x = position.bot.x;
    scene.add(textoverlay.clone());
}

function createTextArray(letter, num, height)
{
    var tempobj = new THREE.Object3D();
    for (var i=0; i<num; i++)
    {
        let text = createText(letter, height*.8);
        text.position.y = -0.35*height;
        text.position.x = -num/2 + i + .1;
        tempobj.add(text);
    }
    tempobj.name = "wipe";
    return tempobj;
}

function createText(textstring, size)
{
    if(loadedFont === undefined )
    {
        console.log("font undefined");
        return ;
    }
    var parameters = {
        font: loadedFont, 
        size: size,
        height: 5
    };
//    var shape = new THREE.FontUtils.generateShapes("Hello World", parameters);
//    var geometry = new THREE.ShapeGeometry(shape);
    var geometry = new THREE.TextGeometry(textstring, parameters);
    var properties = {
        color: new THREE.Color(.5,.5,.5),
//        wireframe: true,
//        opacity: .5,
//        transparent: true
    };
    var material = new THREE.MeshBasicMaterial(properties);
    var text = new THREE.Mesh(geometry, material);
    var tempobj = new THREE.Object3D();
    tempobj.add(text);
    return tempobj;
}

function midlineAxes()
{
    var length = w;
    var geometry = new THREE.PlaneGeometry(length, .05);
    var properties = {
        color: new THREE.Color(1,1,1)
    };
    var material = new THREE.MeshBasicMaterial(properties);
    var mesh = new THREE.Mesh(geometry, material);
    scene.add(mesh.clone());
    mesh.rotation.z = Math.PI/2;
    scene.add(mesh.clone());
}

function findtextures(data, luminance = undefined)
{
    var obj = {};
    if (luminance !== undefined)
    {
        var lig = luminance === 1? "light": "dark";
        var variation = pickvar(5);
    }
    else
    {
        var variation = pickvar(10);
        var lig = "";
    }
    var name = String(data.length+7)+variation+lig;
//    console.log(name);
    for(let material of textures)
    {
        if(material.map.name === name)
        {
            obj = material;
        }
    }
    return obj;
}
function pickvar(n)
{
    //returns random letter within the first n letters. used to pick random variation of bar
    var alphastring = 'abcdefghijklmnopqrstuvwxyz';
    var numVariations = n;
    var pick = Math.floor(Math.random()*numVariations);
    return alphastring[pick];
}

/*running the experiment*/
function nextTrial()
{
    //function handles recording results and setting up next trial
    var test = globallookup('data');
    clearTimeout(wipetimer); // stops wipetimer, otherwise will wipe too early next trial
    recResults(test[globallookup('num' )], resultArray); //recording results
    screenwipe(); // wipes screen (except background)
    var num = globallookup('num') ;
    num += 1;
    changeglobal('num', num); //next trial
    if(globallookup('num')<test.length) //check if reached end of trial data
    {
        var timer = setTimeout(createtrial, 500, globallookup('num')); // create next trial
        //timer used to let screen stay blank to signal next trial
    }
    else
    {
        //reset num, new repeat
        changeglobal('num', 0);
        var repeat = globallookup('repeat');
        changeglobal('repeat', repeat+1);
        if(globallookup('repeat')<globallookup('numRepeat')) //check if reached required num of repeats
        {
            var timer = setTimeout(createtrial, 500, globallookup('num'));
        }
        else
        {
            exit(); //POSTs results, ends exp
        }
    }
}
/*mouse and keyboard controls*/
document.onkeydown = function(key)
{
    /*storing variables on key press */
    var run = globallookup('run'); //boolean, is the experiment supposed to be running
    var time = curtime - globallookup('trialStartTime'); //checks for time difference between current time and time of the latest trial. used to prevent spamming responses
    var task = globallookup('task'); //0 or 1, which task is currently activated
    if(!run && key.keyCode === 32) //starting experiment, only recognises space
    {
        changeglobal('run', true); //changes run to true
        toggleVisibility('gobar'); // toggles off splash screen and toggles on instructions for task1 
        toggleVisibility('splashscreen');
        toggleVisibility('instruct1');
        startup(); //starts experiment
    }
    else if((task === 0 && run && time > 500) && (key.keyCode === 37 || key.keyCode === 39))
    {
        //responding during letter task, only recognises arrow keys
        changeglobal('task', 1); //sets task to 1
        toggleVisibility('instruct1'); // toggles off instructions for letter task
        toggleVisibility('instruct2');// toggles on instructions for bar task
        var num = globallookup('num'); //get trial data to check responses
        var data = globallookup('data');
        if(key.keyCode === 37) //check if answer correct
        {
            if (data[num].letter === 'a' || data[num].letter === 'e')
            {
                resultArray = [1];
            }
            else
            {
                resultArray = [0];
            }
        }
        else if(key.keyCode ===39)
        {
            if(data[num].letter === 'a' || data[num].letter === 'e')
            {
                resultArray = [0];
            }
            else
            {
                resultArray = [1];
            }
        }
        var timer = globallookup('trialtimer'); //get display time information
        var trialtimer = Math.floor(Math.random()*(timer.maxTime - timer.minTime))+timer.minTime;
        wipetimer = setTimeout(screenwipe, trialtimer); //set timer to wipe bars
    }
    else if((task === 1 && time > 500 && run)&&(key.keyCode === 38 || key.keyCode === 40))
    {
        //responding during bar task, only recognises arrow keys
        changeglobal('task', 0); // set task to 0
        toggleVisibility('instruct1'); //turns off instructions for bar task, displays instructions for letter task
        toggleVisibility('instruct2');
        let whereleft = globallookup('whereleft'); // get trial information to check responses
        let luminance = globallookup('whereluminance');
        switch(luminance){
            case 0:
                var whereluminance = "right"
                break;
            case 1:
                var whereluminance = "left"
                break;
            default:
                var whereluminance = "none"
                break;
        }
        console.log(whereluminance);
        var leftdark = whereleft === 0?"top":"bot";
        if(key.keyCode === 38)// check and then store responses
        {
            changeglobal('trialStartTime', Date.now()); // change trial start timer to now
            resultArray.push('top', leftdark, whereluminance); // push responses to result array
            nextTrial(); // start next trial
        }
        else if(key.keyCode === 40)
        {
            changeglobal('trialStartTime', Date.now());
            resultArray.push('bot', leftdark, whereluminance);
            nextTrial();
        }
    }
};

function toggleVisibility(id)
{
    console.log('toggleing: ' + id )
    var e = document.getElementById(id);
    e.style.display = e.style.display !== "block"?"block":"none";
}

function exit()
{
    outputResponses();
//    window.location.reload();
}
function screenwipe()
{
    for(var i=scene.children.length-1; i>=0; i--)
    {
        if (scene.children[i].name === "wipe" || scene.children[i].name === "filter")
        {
            scene.remove(scene.children[i]);
        }
    }
    console.log('wiping', curtime - globallookup('trialStartTime'))
}


/************************END WORKSPACE************************/
var curtime;
function render() { 
    curtime = Date.now();
    
    requestAnimationFrame( render ); 
    renderer.render( scene, camera ); 
    
} 
render();
